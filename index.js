/**
 * Copyright © 2020 Cineplex Digital Media, Inc.
 */
'use strict'

const build = require('@flex/smart-engine-dispatcher')
const death = require('death')

const onProcessKilled = death({
  SIGHUP: true,
  uncaughtException: true
});

(async function main () {
  process.title = 'SmartEngine Lite Promotion Scoring Dispatcher Service'
  onProcessKilled((signal, err) => {
    switch (signal) {
      case 'SIGINT':
        console.log('Received SIGINT signal.')
        process.exit(128 + 2)
      case 'SIGTERM':
        console.log('Received SIGTERM signal.')
        process.exit(0)
      case 'SIGHUP':
        console.log('Received SIGHUP signal.')
        process.exit(0)
      case 'uncaughtException':
        console.error('Uncaught exception.', {
          error: err,
          stacktrace: err.stack
        })
        console.error(err.stack)
        process.exit(1)
      default:
        console.log('Received ' + signal + ' signal.')
        break
    }
  })

  try {
    console.log('Starting SmartEngine Dispatcher Web Service')
    console.log('Webserver on port: %d', 9293)
    const server = build({ logger: true })

    // Use '::' to listen on all ipv6 and ipv4 interfaces.
    await server.listen(9293, '::')

    console.log('Started SmartEngine Dispatcher Web Service')
  } catch (err) {
    console.error('Uncaught error while running service: ', err)
    process.exit(1)
  }
})()
