FROM node:12

WORKDIR /app

COPY package.json yarn.lock ./

RUN npm config set registry https://repo.tech.ek3.com/repository/public-npm/
RUN yarn install

COPY index.js config.json ./

EXPOSE 9293

CMD ["yarn", "start"]