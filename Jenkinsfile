pipeline {
  environment {
    registryHost = "repo.tech.ek3.com:18087"
    registry = "flex/smart-engine-dispatcher"
    packageVersion = "unknownVersion"
  }

  agent {
    label 'docker'
  }

  stages {
    stage("Determine package version") {
      agent {
        docker {
          image 'centos7node:1.0'
        }
      }
      steps {
        script {
          packageVersion = sh(
            script: 'npm run version --silent',
            returnStdout: true
          ).trim()
        }
      }
    }
    
    stage("Build Docker Image") {
      steps {
        script {
          dockerImage = docker.build registryHost + "/" + registry + ":$BRANCH_NAME-" + packageVersion 
        }
      }
    }

    stage("Deploy Image To Registry") {
      steps {
        script {
          // 22 refers to the id label of the jenkins credentials
          // TODO we need a when clause to deal with actual releases
          docker.withRegistry("https://" + registryHost, '22') {
            dockerImage.push()
          }
        }
      }
    }
  }

  post {
    cleanup {
      sh "docker rmi ${dockerImage.imageName()}"
      deleteDir() /* clean workspace */
    }
  }
}